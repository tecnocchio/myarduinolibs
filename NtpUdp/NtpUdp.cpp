#include "NtpUdp.h"

NtpUdp::NtpUdp() {}

void NtpUdp::begin() {
  udp.begin(123);  // NTP uses port 123
}

time_t NtpUdp::getTime() {
  byte packetBuffer[48];

  // Initialize request packet
  memset(packetBuffer, 0, sizeof(packetBuffer));
  packetBuffer[0] = 0b11100011;  // LI, Version, Mode
  packetBuffer[1] = 0;           // Stratum
  packetBuffer[2] = 6;           // Polling Interval
  packetBuffer[3] = 0xEC;        // Precision

  // Send NTP request
  if (udp.beginPacket("pool.ntp.org", 123) == 0 || udp.write(packetBuffer, sizeof(packetBuffer)) == 0 || udp.endPacket() == 0) {
    Serial.println(F("Failed to send NTP request"));
    return 0;
  }

  // Receive NTP response
  if (udp.parsePacket() == 0 || udp.read(packetBuffer, sizeof(packetBuffer)) == 0) {
    Serial.println(F("Failed to receive NTP response"));
    return 0;
  }

  // Extract and return timestamp
  unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
  unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
  unsigned long secsSince1900 = highWord << 16 | lowWord;
  const unsigned long seventyYears = 2208988800UL;
  time_t epoch = secsSince1900 - seventyYears;

  return epoch;
}

String NtpUdp::formatTime(time_t timestamp, const char* format) {
  struct tm* timeinfo;
  char buffer[20];

  // Get local time
  timeinfo = localtime(&timestamp);

  // Format time using the provided format string
  strftime(buffer, sizeof(buffer), format, timeinfo);

  return String(buffer);
}