#ifndef NTPUDP_H
#define NTPUDP_H

#include <Arduino.h>
#include <WiFiUdp.h>

class NtpUdp {
public:
  NtpUdp();
  void begin();
  time_t getTime();
  String formatTime(time_t timestamp, const char* format);

private:
  WiFiUDP udp;
};

#endif  // NTP_H