// DHTSensor.cpp
#include "DHTSensor.h"


DHTSensor::DHTSensor(uint8_t pin, uint8_t type)
  : _dht(pin, type) {
  _dht.begin();
}

float DHTSensor::getHumidity() {
  float h = _dht.readHumidity();
  if (isnan(h)) {
    return -1;  // Returns -1 if the reading failed
  } else {
    return h;
  }
}

float DHTSensor::getTemperature() {
  float t = _dht.readTemperature();
  if (isnan(t)) {
    return -1;  // Returns -1 if the reading failed
  } else {
    return t;
  }
}

float DHTSensor::getComputeHeatIndex() {
  float t = _dht.readTemperature();
  float h = _dht.readHumidity();
  float tp = _dht.computeHeatIndex(t, h, false);
  if (isnan(tp)) {
    return -1;  // Returns -1 if the reading failed
  } else {
    return tp;
  }
}

std::string DHTSensor::getAllText() {
  float t = _dht.readTemperature();
  float h = _dht.readHumidity();
  float tp = _dht.computeHeatIndex(t, h, false);
  char str[30];  // Buffer size may need adjustment based on expected temperature/humidity values
  snprintf(str, sizeof(str), "T %.1f\nH %.1f%%\nP %.1f°", t, h, tp);
  return std::string(str);
}
std::string DHTSensor::getT() {
  float t = _dht.readTemperature();
  char str[6];  // Buffer size may need adjustment based on expected temperature/humidity values
  snprintf(str, sizeof(str), "%.1f", t);
  return std::string(str);
}
std::string DHTSensor::getH() {
  float h = _dht.readHumidity();
  char str[6];  // Buffer size may need adjustment based on expected temperature/humidity values
  snprintf(str, sizeof(str), "%.1f", h);
  return std::string(str);
}
std::string DHTSensor::getTP() {
  float h = _dht.readHumidity();
  float t = _dht.readTemperature();
  float tp = _dht.computeHeatIndex(t, h, false);
  char str[6];  // Buffer size may need adjustment based on expected temperature/humidity values
  snprintf(str, sizeof(str), "%.1f", tp);
  return std::string(str);
}
