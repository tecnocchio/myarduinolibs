// DHTSensor.h
#ifndef DHTSensor_h
#define DHTSensor_h

#include "Arduino.h"
#include <DHT.h>
#include <string>

class DHTSensor {
  public:
    DHTSensor(uint8_t pin, uint8_t type);
    float getHumidity();
    float getTemperature();
    float getComputeHeatIndex();
    std::string getAllText();
	std::string getT();
	std::string getH();
	std::string getTP();
  private:
    DHT _dht;
};

#endif