#include "EInkDisplay.h"
#include "DHTSensor.h"

EInkDisplay eInkDisplay;
DHTSensor dhtSensor(13, DHT22);  // Adjust pin as needed

void setup() {
  Serial.begin(115200);
  eInkDisplay.init();
}

void loop() {
  eInkDisplay.setTextSize(2);
  eInkDisplay.printInk(dhtSensor.getAllText().c_str(), 0, 0);
  delay(10000);  // Delay between updates
}
