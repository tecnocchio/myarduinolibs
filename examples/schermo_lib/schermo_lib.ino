
#include "Screen.h"
#include "Wifi8266.h"
#include "NtpUdp.h"


Screen myScreen;  // Uses default values
Wifi8266 myWifi;
NtpUdp ntp;

void setup() {
  Serial.begin(9600);
  myScreen.init();
  myScreen.write("connecting...", 0, 0);
  myWifi.init("*****", "***", IPAddress(0, 0, 0, 0), IPAddress(0, 0, 0, 0), IPAddress(0, 0, 0, 0));
  String ip = myWifi.getLocalIPString();
  myScreen.write(ip, 0, 0);
  ntp.begin();
}


void loop() {
  static unsigned long lastNtpUpdate = 0;
  static unsigned long lastDisplayUpdate = 0;
  time_t currentTime = 0;
  while (currentTime == 0) {
    currentTime = ntp.getTime();
  }
  while (true) {
    // Update NTP time every 60 seconds
    if (millis() - lastNtpUpdate >= 60000) {
      currentTime = ntp.getTime();
      Serial.println(currentTime);
      lastNtpUpdate = millis();
    }
    // Update displayed time every second
    if (millis() - lastDisplayUpdate >= 1000) {
      currentTime = currentTime + 1;  // Assuming you're using the TimeLib library
      myScreen.write(ntp.formatTime(currentTime, "%H:%M:%S"), 32, 16);
      lastDisplayUpdate = millis();
    }
    delay(100);
  }
}
