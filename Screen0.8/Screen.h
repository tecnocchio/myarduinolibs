// Screen.h
#ifndef SCREEN_H
#define SCREEN_H

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SSD1306_SWITCHCAPVCC 0x02  ///< Gen. display voltage from 3.3V
#define SCREEN_ADDRESS 0x3C

class Screen {
public:
  Screen(
    int width = 128,
    int height = 64,
    int sdaPin = 14,
    int sclPin = 12,
    int resetPin = -1);

  void init();
  void write(const String& text);
  void write(const String& text, const int x, const int y);

private:
  Adafruit_SSD1306 display;
};

#endif  // SCREEN_H