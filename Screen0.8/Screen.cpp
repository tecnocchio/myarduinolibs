// Screen.cpp
#include "Screen.h"
#include <Adafruit_SSD1306.h>


Screen::Screen(
  int width,
  int height,
  int sdaPin,
  int sclPin,
  int resetPin)
  : display(width, height, &Wire, resetPin) {
  // Constructor
  Wire.begin(sdaPin, sclPin);
}

void Screen::init() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("Error initializing the OLED screen"));
    while (true)
      ;
  }

  //   display.display();
  //   delay(2000);
  display.clearDisplay();
}

void Screen::write(const String& text, const int x, const int y) {
  int txtLen = text.length();

  // Clear the area where the text will be drawn
  display.fillRect(x, y, txtLen * 6, 8, SSD1306_BLACK);
  Serial.print(F("x:"));
  Serial.print(x);
  Serial.print(F(" y:"));
  Serial.println(y);

  // Set text size and color
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);

  // Set the cursor position
  display.setCursor(x, y);

  // Print the text
  display.print(text);

  // Display the content
  display.display();
}

void Screen::write(const String& text) {
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  display.println(text);
  display.display();
}