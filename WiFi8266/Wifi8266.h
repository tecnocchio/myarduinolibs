#ifndef WIFI8266_H
#define WIFI8266_H

#include <ESP8266WiFi.h>

class Wifi8266 {

public:
  Wifi8266();
  void init(const char* ssid, const char* password, const IPAddress& local_ip, const IPAddress& gateway, const IPAddress& subnet);
  String getLocalIPString();
};
#endif