// Wifi8266.cpp
#include "Wifi8266.h"

Wifi8266::Wifi8266() {
  // Constructor implementation, if needed
}

void Wifi8266::init(const char* ssid, const char* password, const IPAddress& local_ip, const IPAddress& gateway, const IPAddress& subnet) {
  // Implementation of the init function
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }

  Serial.println("Connected to WiFi");

  // Set static IP configuration if provided
  if (local_ip != IPAddress(0, 0, 0, 0)) {
    WiFi.config(local_ip, gateway, subnet);
    Serial.println("Static IP configuration set");
  }
}

String Wifi8266::getLocalIPString() {
  return WiFi.localIP().toString();
}