#include "EInkDisplay.h"

EInkDisplay::EInkDisplay() : display(GxEPD2_154_D67(5, 4, 16, 15)) {} // Adjust pin numbers as needed

void EInkDisplay::init() {
    display.init(115200, true, 50, false);
}
void EInkDisplay::setTextSize(uint8_t size) {
    display.setTextSize(size);
}

void EInkDisplay::printInk(const char* str, uint16_t x, uint16_t y) {
    int16_t tbx, tby;
    uint16_t tbw, tbh;
    display.setRotation(1);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);
    display.getTextBounds(str, x, y, &tbx, &tby, &tbw, &tbh);

    display.setPartialWindow(x, y, tbw, tbh);
    display.firstPage();
    do {
        display.fillScreen(GxEPD_WHITE);
        display.setCursor(x, y + (0 - tby));
        display.print(str);
    } while (display.nextPage());

    Serial.print(F("text: "));
    Serial.println(str);
}
