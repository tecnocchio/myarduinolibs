#ifndef EInkDisplay_h
#define EInkDisplay_h

#include <Arduino.h>
#include <GxEPD2_BW.h>  // Include the library for black and white e-ink displays
#include <GxEPD2_3C.h>  // Include the library for 3-color e-ink displays
#include <Fonts/FreeMonoBold9pt7b.h>

class EInkDisplay {
public:
  EInkDisplay();                                           // Constructor
  void init();                                             // Initialize the display
  void printInk(const char* str, uint16_t x, uint16_t y);  // Print text to the display
  void setTextSize(uint8_t size);

private:
  GxEPD2_BW<GxEPD2_154_D67, GxEPD2_154_D67::HEIGHT> display;  // Example for a specific display model
};

#endif